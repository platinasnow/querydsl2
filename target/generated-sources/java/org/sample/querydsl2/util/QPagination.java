package org.sample.querydsl2.util;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPagination is a Querydsl query type for Pagination
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QPagination extends BeanPath<Pagination> {

    private static final long serialVersionUID = 669549211L;

    public static final QPagination pagination = new QPagination("pagination");

    public final NumberPath<Integer> currentItem = createNumber("currentItem", Integer.class);

    public final NumberPath<Integer> currentPage = createNumber("currentPage", Integer.class);

    public final NumberPath<Integer> itemPerPage = createNumber("itemPerPage", Integer.class);

    public final NumberPath<Integer> jumpNextPage = createNumber("jumpNextPage", Integer.class);

    public final NumberPath<Integer> jumpPrevPage = createNumber("jumpPrevPage", Integer.class);

    public final NumberPath<Integer> nextPage = createNumber("nextPage", Integer.class);

    public final NumberPath<Integer> page = createNumber("page", Integer.class);

    public final NumberPath<Integer> pageBegin = createNumber("pageBegin", Integer.class);

    public final NumberPath<Integer> pageCount = createNumber("pageCount", Integer.class);

    public final NumberPath<Integer> pageEnd = createNumber("pageEnd", Integer.class);

    public final NumberPath<Integer> totalItemCount = createNumber("totalItemCount", Integer.class);

    public QPagination(String variable) {
        super(Pagination.class, forVariable(variable));
    }

    public QPagination(Path<? extends Pagination> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPagination(PathMetadata<?> metadata) {
        super(Pagination.class, metadata);
    }

}

