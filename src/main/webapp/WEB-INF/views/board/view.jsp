<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="include.jsp" %>
<style type="text/css"> 
	.contents-wrap{margin:30px 0 0 0;min-height: 500px;}
	.contents{ margin: 60px 0 0 0;}
	.recode-wrap{text-align: right; color: #888;}
	.hit-wrap{color:#888; margin: 10px 0;}
	.viewForm{margin: 20px 0 0 0;}
	.file-wrap{text-align: right; margin-top: 45px;}
</style>
</head>

	<div class="container ">
		<div class="contents panel panel-success">
			<div class="title-wrap panel-heading">
				${item.title }
			</div>	
			<div class="panel-body">
				<div class="recode-wrap">
					${item.user.name } | <fmt:formatDate value="${item.recodeDate }" pattern="yyyy-MM-dd"/> 
				</div>
				<div class="contents-wrap">
				${item.contents }
				</div>
				<div class="hit-wrap">
					<b>조회수</b> ${item.hit }
				</div>
			</div>
		</div>
		<form class="viewForm" method="post">
			<input type="hidden" name="seq" value="${item.seq }" />
			<input type="hidden" name="boardNum" id="boardNum" value="${board.boardNum }" />
			<button type="button" onclick="onModify()" class="btn btn-primary">수정</button>
			<button type="button" onclick="onList()" class="btn btn-primary">목록</button>
		</form>	
	</div>
	
<script type="text/javascript">
var form = document.forms[0];
	var onModify = function(){
		form.action = '/board/modify';
		form.submit();
	};
	
	var onList = function(){
		form.action = '/board/list/'+'${board.boardNum }';
		form.submit();
	};
</script>


