package org.sample.querydsl2.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.sample.querydsl2.util.Pagination;

@Entity
public class Board extends Pagination implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer seq = 0;
	String title;
	String contents;
	Date recodeDate;
	Integer hit = 0;
	Integer author;
	Integer boardNum = 1;

	@JoinColumn(name = "author", referencedColumnName = "pn", insertable = false, updatable = false)
	@ManyToOne
	User user;

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Date getRecodeDate() {
		return recodeDate;
	}

	public void setRecodeDate(Date recodeDate) {
		this.recodeDate = recodeDate;
	}

	public Integer getHit() {
		return hit;
	}

	public void setHit(Integer hit) {
		this.hit = hit;
	}

	public Integer getAuthor() {
		return author;
	}

	public void setAuthor(Integer author) {
		this.author = author;
	}

	public Integer getBoardNum() {
		return boardNum;
	}

	public void setBoardNum(Integer boardNum) {
		this.boardNum = boardNum;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Board [seq=" + seq + ", title=" + title + ", contents=" + contents + ", recodeDate=" + recodeDate + ", hit=" + hit + ", author="
				+ author + ", boardNum=" + boardNum + ", user=" + user + "]";
	}

}
