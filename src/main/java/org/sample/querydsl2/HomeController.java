package org.sample.querydsl2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.sample.querydsl2.domain.Board;
import org.sample.querydsl2.service.BoardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequestMapping("/board")
@Controller
public class HomeController {
	
	@Autowired
	private BoardService boardService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/")
	public String home(Model model, Board board) {
		
		return "index";
	}
	
	@RequestMapping(value = "/list/{boardNum}")
	public String list(Model model, Board board) {
		board.setTotalItemCount(boardService.boardTotalCount(board));
		//model.addAttribute("list", boardService.findJoinBoard(board));
		model.addAttribute("list", boardService.findJoinBoardNative(board));
		
		return "/board/list";
	}
	
	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public String write(Model model, Board board) {
		return "/board/write";
	}

	@RequestMapping(value = "/writeSubmit", method = RequestMethod.POST)
	public String writeSubmit(Model model, Board board, RedirectAttributes redirectAttributes) {
		board.setAuthor(4);
		board.setRecodeDate(new Date());
		System.out.println("board : "+board.toString());
		
		int seq = boardService.insertBoardItem(board);

		redirectAttributes.addFlashAttribute("board", board);
		return "redirect:view/"+seq;
	}

	@RequestMapping(value = "/view/{seq}")
	public String view(Model model, Board board, RedirectAttributes redirectAttributes) {
		Board item = boardService.getBoardItem(board);
		if(item == null){
			redirectAttributes.addFlashAttribute("error", "잘못된 접근입니다.");
			return "redirect:/error";
		}
		model.addAttribute("item", item);
		return "/board/view";
	}

	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public String modify(Model model, Board board) {
		List<Integer> fileSeq = new ArrayList<Integer>();
		
		model.addAttribute("item", boardService.getBoardItem(board));
		model.addAttribute("modify", "true");
		model.addAttribute("fileSeq", fileSeq.toString().replace("[", "").replace("]", ""));
		return "/board/write";
	}

	@RequestMapping(value = "/modifySubmit", method = RequestMethod.POST)
	public String modifySubmit(Model model, Board board, RedirectAttributes redirectAttributes) {
		//board.setContents(board.getContents().replaceAll("\n", "").replaceAll("\t", "").replaceAll("\r", "").replaceAll("'", "&apos;"));
		Board boardItem =  boardService.getBoardItem(board);
		System.out.println("item :" + boardItem);
		Board updateItem = board;
		boardItem.setTitle(updateItem.getTitle());
		boardItem.setContents(updateItem.getContents());
		boardService.updateBoardItem(boardItem);
		redirectAttributes.addFlashAttribute("board", board);
		return "redirect:view/"+board.getSeq();
	}
	
}
